<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\BookSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="book-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="row">
        <div class="col-sm-4">
            <?= $form->field($model, 'author_id')->dropDownList(\common\models\Author::getAuthorsList(), ['prompt' => 'Выбрать...']) ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'name') ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <?php  echo $form->field($model, 'from_date') //TODO need datepicker?>
        </div>
        <div class="col-sm-4">
            <?php  echo $form->field($model, 'to_date') //TODO need datepicker?>
        </div>
        <div class="col-sm-4 text-right">
            <br/>
            <?= Html::submitButton('Поиск', ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Очистить фильтры', ['index'], ['class' => 'btn btn-info']) ?>
        </div>
    </div>



    <?php ActiveForm::end(); ?>

</div>
