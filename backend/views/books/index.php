<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel common\models\BookSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Books';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="book-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Book', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
               'attribute' => 'id',
                'headerOptions' => ['width' => '50'],
            ],
            'name',
            [
                'label'=>'preview',
                'format'=>'raw',
                'value' => function($data){
                    return Html::beginTag('a', ['href'=>$data->getPreviewURL(), 'data-lightbox'=>$data->id]) .
                            Html::img($data->getPreviewURL(),['alt'=>'preview', 'width'=>'70px', 'height'=>'100px']) .
                           Html::endTag('a');
                }
            ],
            [
                'attribute' => 'authorName',
                'label' => 'Автор'
            ],
            [
                'attribute' => 'date',
                'format' =>  ['date', 'php:Y-m-d'],
            ],
            'date_update',

            [
                'class' => 'yii\grid\ActionColumn',
                'options' => ['style' => 'width:100px;'],
                'template' => '{view} {update} {delete}',
                'buttons' => [
                    'update' => function ($url,$model) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-pencil"></span>',
                            $url, ['target' => '_blank']);
                    },
                    'view' => function ($url,$model,$key) {
                        //TODO put js to  .js files
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                            'title' => Yii::t('yii', 'Close'),
                            'onclick'=>"
                                $.ajax({
                                    type:'GET',
                                    cache: false,
                                    url: '". $url ."',
                                    success: function(response) {
                                        console.log(response);
                                        $('#details .modal-body').html(response);
                                        $('#details').modal('show');
                                    }
                                });
                            return false;",
                        ]);
                    },
                ],
            ],

        ],
    ]); ?>

</div>

<?php
    Modal::begin([
        'id' => 'details',
        'toggleButton' => false
    ]);

Modal::end(); ?>


