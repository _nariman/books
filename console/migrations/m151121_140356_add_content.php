<?php

use yii\db\Schema;
use yii\db\Migration;

class m151121_140356_add_content extends Migration
{
    public function up()
    {
        $this->batchInsert('authors', ['first_name', 'last_name'],[
            ['Рэй', 'Брэдбери'],
            ['Редьярд', 'Киплинг'],
            ['Джек', 'Лондон'],
        ]);
    }

    public function down()
    {
        echo "m151121_140356_add_content cannot be reverted.\n";

        $this->truncateTable('authors');
       //return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
