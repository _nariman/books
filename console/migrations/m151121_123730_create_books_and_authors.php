<?php

use yii\db\Schema;
use yii\db\Migration;

class m151121_123730_create_books_and_authors extends Migration
{
    public function up()
    {
        $this->createTable('books', [
            'id' => 'pk',
            'name' => 'VARCHAR(100)' ,
            'date_create' => 'DATETIME',
            'date_update' => 'DATETIME',
            'preview' => 'VARCHAR(32)',
            'date' => 'DATETIME',
            'author_id' => 'INT',
        ]);

        $this->createTable('authors', [
            'id' => 'pk',
            'first_name' => 'VARCHAR(30)',
            'last_name' => 'VARCHAR(30)',
        ]);
    }

    public function down()
    {
        echo "m151121_123730_create_books_and_authors cannot be reverted.\n";
        $this->dropTable('books');
        $this->dropTable('authors');
        //return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
